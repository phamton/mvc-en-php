<?php
    
    class View{
        
        function render($controller,$view){
            $controller = get_class($controller);
            
            // trae la vista q esta guardada en viewa/nombrecontrolador DE la vista ej USUARIO/elnombre de la vista
            require './views/'.$controller.'/'.$view.'.php';
            
        }
    }

