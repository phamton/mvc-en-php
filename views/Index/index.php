<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>Mi MVC Básico en PHP</title>
        <link rel="stylesheet" type="text/css" href="<?php echo URL; ?>public/css/stylesheet.css">
        <script src="<?php echo URL; ?>public/js/jquery-3.0.0.js"></script>
    </head>
    <body>
        <?php if(!Session::exist()){ ?>
        <div id ="formWrapper">
            
            <div class="formWrapper">
                <div class="formTitle">Entrar</div>
                <form name="signIn"  action="<?php echo URL;?>User/signIn" method="POST">
                    <input name="username" type="text" placeholder="Username" required/>
                    <input name="password" type="text" placeholder="Password" required/>
                    <input id="signInBtn" name="signInBtn" type="submit" value="Entrar" />
                    <div class="smallText">
                        <span> ¿No estas Registrado? <div class="button"id="signUpButton">Registrate Aqui</div></span>
                        <span> ¿Olvidaste tu Password? <a href="">Recordar Password</a> </span>
                    </div>
                </form>
            </div>
            
            
            <div class="formWrapper hidden">
                <div class="formTitle">Registro</div>
                <form name="signUp"  action="<?php echo URL;?>User/signUp" method="POST">
                    <input name="name" type="text" placeholder="Nombre" required/>
                    <input name="username" type="text" placeholder="Username" required/>
                    <input name="email" type="email" placeholder="Pepito@gmail.com" required/>
                    <input name="password" type="password" placeholder="Password" required/>
                    <input id="signUpBtn" name="signUpBtn" type="submit" value="Registrarme" />
                    <div class="smallText">
                        <span> ¿Si estas Registrado? <div class="button"id="signInButton">Volver</div></span>
                        
                    </div>
                </form>
            </div>
            
        </div>
        
    <script>
        $(function(){

		$('#signUpButton').click(function(){

			$('form[name=signIn]').parent().hide();
			$('form[name=signUp]').parent().fadeToggle();
		});

		$('#signInButton').click(function(){

			$('form[name=signUp]').parent().hide();
			$('form[name=signIn]').parent().fadeToggle();
		});

				
                $('#signUpBtn').click(function(e){

			e.preventDefault();
			signUp();

		});


		$('#signInBtn').click(function(e){

			e.preventDefault();
			signIn();

		});
                
	});

         function signUp(){
                
                var name = $('form[name=signUp] input[name=name]')[0].value;
                var username = $('form[name=signUp] input[name=username]')[0].value;
                var email = $('form[name=signUp] input[name=email]')[0].value;
                var password = $('form[name=signUp] input[name=password]')[0].value;
                
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>User/signUp",
                    data: {name: name, username: username, email: email, password: password}
                }).done(function(response){
                   
                    
                    if(response == true){
                        alert("Registro Exitoso!");
                    }else{
                        alert(response);
                    }
                });
                
    
    
                }
          
	    function signIn(){
                    
                
                var username = $('form[name=signIn] input[name=username]')[0].value;
                var password = $('form[name=signIn] input[name=password]')[0].value;
                
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>User/signIn",
                    data: {username: username, password: password}
                }).done(function(response){
                   
                    
                    if(response == 1){
                        location.reload();
                        
                    }else{
                        alert("Usuario o Password incorrectos");
                    }
                });
                    
		}

            
    </script>
        
        <?php }else { ?>
    <div class="formWrapper" >
        <?php echo Session::getValue('U_NAME');?>
        <button id="closeSessionBtn">Cerrar Sesión</button>
        <script>
            $(function(){
                $('#closeSessionBtn').click(function(){
                    document.location = "<?php echo URL; ?>User/destroySession";
                });
            });
        </script>
    </div>
        <?php }?>
    </body>
</html>